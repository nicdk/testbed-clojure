(dfn blank? [str]
     (every? #(CAracter/isWhitespace %) str))

(defrecord Person [first-name last-name])

(def foo (->Person "Aaron" "Bedra"))

(defrecord name [arg1 arg2 arg3])

(defrecord name [Type :arg1 Type :arg2 Type :arg3]
  :allow-nulls false)

(defn hello-world [username]
  (println (format "Hello, %s" username)))

(cond (= x 10) "equal"
      (> x 10) "more")

(for [c compositions :when (= "Requiem" (:name c))]
  (:composer c))

(def accounts (ref #{}))
(defrecord Account [id balance])

(dosync
 (alter accounts conj (->Account "CLJ" 1000.00)))

(System/getProperties)

(.. "hello" getClass getProtectionDomain)

(.start (new Thread (fn [] (println "Hello" (Thread/currentThread)))))


